
var scroll_element,
    scroll_y;

scroll_element = document.getElementsByClassName('header-main')[0];

if(!!scroll_element) {

    scroll_y = position_y(scroll_element);
}

setContentScroll();

window.addEventListener('scroll', () => {
    
    setContentScroll();
})

function setContentScroll() {

    if(window.pageYOffset >= scroll_y && !!header_main) {
        
        header_main.classList.add('scrollwindow');
    }
    else {

        header_main.classList.remove('scrollwindow');
    }
}