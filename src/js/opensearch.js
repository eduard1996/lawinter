
var search, search_show, search_close, search_close_desctop;

search_show = document.getElementsByClassName('js-search-show');

search_close = document.getElementsByClassName('header-search-close');

search_close_desctop = document.getElementsByClassName('search-close');

search = document.getElementsByClassName('search')[0];

my_events(search_show, 'click', function() {

    var width_scrollbar;

    width_scrollbar = overflow_hidden();

    search.style.cssText = `

        right: -${width_scrollbar}px;
    `;

    header_main.classList.add('searchshow');
    header_main.classList.remove('searchhide');
})

my_events(search_close, 'click', function() {

    if(window.innerWidth < 992) {

        overflow_visible();
        
        performanceSearchHide();
    }
})

my_events(search_close_desctop, 'click', function() {

    if(window.innerWidth >= 992) {

        overflow_visible();

        performanceSearchHide();
    }
})

function performanceSearchHide() {

    search.style.cssText = `

        right: 0;
    `;

    header_main.classList.add('searchhide');
    header_main.classList.remove('searchshow');
}