document.addEventListener('DOMContentLoaded', () => {
    
    //=include variables.js
    //=include modals.js
    //=include togglemenu.js
    //=include togglecategory.js
    //=include scrolldocument.js
    //=include sliders.js
    //=include opensearch.js
    //=include gallery.js
    //=include products.js
    //=include filters.js
    //=include forms.js
})