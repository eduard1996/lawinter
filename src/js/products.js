var products_four, 
    products_three,
    products_main;

products_four = document.getElementsByClassName('products-filter-four');

products_three = document.getElementsByClassName('products-filter-three');

products_main = document.getElementsByClassName('js-products-view')[0];

my_events(products_four, 'click', function($this) {

    products_three[0].classList.remove('active');
    $this.classList.add('active');

    products_main.classList.add('productsfour');
    products_main.classList.remove('productsthree');
})

my_events(products_three, 'click', function($this) {

    products_four[0].classList.remove('active');
    $this.classList.add('active');

    products_main.classList.remove('productsfour');
    products_main.classList.add('productsthree');
})