var modal_open = 'modal--open';

var gallery_close, gallery_modal, gallery_show, swiper_card;

gallery_close = document.getElementsByClassName('modal-close');

gallery_modal = document.getElementsByClassName('js-modal-gallery');

gallery_show = document.getElementsByClassName('product-main-see');

// ~ Открытие модального окна

my_events(gallery_show, 'click', function() {

    var swiper_gallery, gallery_count;

    gallery_modal[0].classList.add('showmodal');
    gallery_modal[0].classList.remove('closemodal');

    // Инициализация слайдера

    gallery_count = gallery_modal[0].querySelectorAll('.gallery-frame').length;

    if(gallery_count <= 4) {

        document.getElementsByClassName('gallery')[0].classList.add('minfifthswipe');
    }

    swiper_gallery = new Swiper('.js-init-gallery', {
        direction: 'vertical',
        slidesPerView: 4,
        navigation: {
            nextEl: '.js-gallery-next',
            prevEl: '.js-gallery-prev',
        },
    });

    overflow_hidden();
})

// ~ Закрытие модального окна

my_events(gallery_close, 'click', function() {

    gallery_modal[0].classList.add('closemodal');
    gallery_modal[0].classList.remove('showmodal');

    setTimeout(function() {
        
        overflow_visible();
    }, 300)
})

my_events(gallery_modal, 'click', function($this, event) {

    var $element, $arrow_prev, $arrow_next;

    $element = document.getElementsByClassName('js-modal-gallery')[0].querySelector('.modal-body');

    $arrow_prev = document.getElementsByClassName('see-prev')[0];

    $arrow_next = document.getElementsByClassName('see-next')[0];

    if(!$element.contains(event.target) && !$arrow_prev.contains(event.target) && !$arrow_next.contains(event.target)) {

        setTimeout(function() {
        
            overflow_visible();
        }, 300)

        gallery_modal[0].classList.add('closemodal');
        gallery_modal[0].classList.remove('showmodal');
    }
})

swiper_card = new Swiper('.js-init-card', {
    direction: 'vertical',
    slidesPerView: 4,
    navigation: {
        nextEl: '.js-card-next',
        prevEl: '.js-card-prev',
    },
});