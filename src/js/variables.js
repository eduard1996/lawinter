var header_main;

header_main = document.getElementsByClassName('header')[0];

// ~

function my_events($element, $event, $content) {

    if(!!$element[0]) {

        for(var i = 0; i < $element.length; i++) {

            $element[i].addEventListener($event, function(event) {

                $content(this, event);
            })
        }
    }
}

function repeat_for($element, $content) {

    if(!!$element[0]) {

        for(var i = 0; i < $element.length; i++) {

            $content($element[i]);
        }
    }
}

function position_y($elem) {
    
    var box, 
        body, 
        docEl, 
        scrollTop, 
        clientTop, 
        top;

    box = $elem.getBoundingClientRect();

    body = document.body;

    docEl = document.documentElement;
  
    scrollTop = window.pageYOffset || docEl.scrollTop || body.scrollTop;

    clientTop = docEl.clientTop || body.clientTop || 0;

    top = box.top + scrollTop - clientTop;
  
    return top;
}

function overflow_hidden () {

    var s;

    s = window.innerWidth - document.documentElement.clientWidth;

    document.body.classList.add('modal--open');

    document.body.style.cssText = `

        padding-right: ${s}px;
    `

    return s;
}

function overflow_visible () {

    document.body.classList.remove('modal--open');

    document.body.style.cssText = `

        padding-right: 0px;
    `
}