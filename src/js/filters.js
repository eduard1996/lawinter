var filters_header, filters_content;

filters_header = document.getElementsByClassName('filters-header');

filters_content = document.getElementsByClassName('filters-content')[0];

my_events(filters_header, 'click', function($this) {

    var $height;

    $height = filters_content.querySelector('.container').offsetHeight;

    if(!$this.classList.contains('active')) {

        filters_content.style.cssText = `

            max-height: ${$height}px;
        `;

        setTimeout(function(){

            filters_content.style.cssText = `

                max-height: none;
            `;
        }, 500)

        $this.classList.add('active');
    }
    else {

        filters_content.style.cssText = `

            max-height: ${$height}px;
        `;

        setTimeout(function() {
            filters_content.style.cssText = `

                max-height: 0px;
            `;

            $this.classList.remove('active');
        }, 50)
    }
})

var filter_header;

filter_header = document.getElementsByClassName('filter-header');

my_events(filter_header, 'click', function($this) {

    var $content, $height;

    $content = $this.parentNode.querySelector('.filter-collapse');

    if(!$this.parentNode.classList.contains('active')) {

        $height = $this.parentNode.querySelector('.filter-content').offsetHeight;

        repeat_for(filter_header, function($this_item) {

            $this_item.parentNode.classList.remove('active');

            $this_item.parentNode.querySelector('.filter-collapse').style.cssText = `
                max-height: 0px;
            `;
        })

        $content.style.cssText = `
            max-height: ${$height}px;
        `;

        $this.parentNode.classList.add('active');
    }
    else {

        $content.style.cssText = `
            max-height: 0px;
        `;

        $this.parentNode.classList.remove('active');
    }
    
})

document.addEventListener('click', (event) => {

    var $element;

    $element = document.getElementsByClassName('filter');

    repeat_for($element, function($item) {

        if(!$item.contains(event.target)) {

            $item.querySelector('.filter-collapse').style.cssText = `
                max-height: 0px;
            `;

            $item.classList.remove('active');
        }
    })
})

// ~ Выбор селекта

var filter_item;

filter_item = document.getElementsByClassName('filter-item');

my_events(filter_item, 'click', function($this) {

    var $parent, $value, $data, $attr;

    $parent = $this.parentNode.parentNode.parentNode;

    $data = $this.textContent;

    $value = $parent.querySelector('[data-main]');

    repeat_for($parent.querySelectorAll('.filter-item'), function($item){

        $item.classList.remove('active');
    })

    $attr = $this.getAttribute('data-value');
    
    $this.classList.add('active');

    $value.innerText = $data;

    $value.setAttribute('data-main', $attr);

    $parent.classList.remove('active');

    $parent.querySelector('.filter-collapse').style.cssText = `
        max-height: 0px;
    `;
})