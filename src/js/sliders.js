var swiper_first,
    swiper_second,
    swiper_third;

swiper_first = new Swiper('.js-init-products-first', {
    effect: 'coverflow',
    slidesPerView: 4,
    centeredSlides: false,
    speed: 600,
    spaceBetween: 20,
    coverflowEffect: {
      rotate: 0,
      depth: 0,
      slideShadows : false,
    },
    breakpoints: {
      1299: {
        slidesPerView: 3,
      },
      991: {
        spaceBetween: 0,
        slidesPerView: 2,
        centeredSlides: true,
        coverflowEffect: {
          rotate: 50,
          depth: 100,
        },
      },
      620: {
        slidesPerView: 1,
      }
    },
    navigation: {
      nextEl: '.js-products-next-first',
      prevEl: '.js-products-prev-first',
    },
});

swiper_second = new Swiper('.js-init-products-second', {
    effect: 'coverflow',
    slidesPerView: 4,
    centeredSlides: false,
    speed: 600,
    spaceBetween: 20,
    coverflowEffect: {
      rotate: 0,
      depth: 0,
      slideShadows : false,
    },
    breakpoints: {
      1299: {
        slidesPerView: 3,
      },
      991: {
        spaceBetween: 0,
        slidesPerView: 2,
        centeredSlides: true,
        coverflowEffect: {
          rotate: 50,
          depth: 100,
        },
      },
      620: {
        slidesPerView: 1,
      }
    },
    navigation: {
      nextEl: '.js-products-next-second',
      prevEl: '.js-products-prev-second',
    },
});

swiper_third = new Swiper('.js-init-products-third', {
    effect: 'coverflow',
    slidesPerView: 4,
    centeredSlides: false,
    speed: 600,
    spaceBetween: 20,
    coverflowEffect: {
      rotate: 0,
      depth: 0,
      slideShadows : false,
    },
    breakpoints: {
      1299: {
        slidesPerView: 3,
      },
      991: {
        spaceBetween: 0,
        slidesPerView: 2,
        centeredSlides: true,
        coverflowEffect: {
          rotate: 50,
          depth: 100,
        },
      },
      620: {
        slidesPerView: 1,
      }
    },
    navigation: {
      nextEl: '.js-products-next-third',
      prevEl: '.js-products-prev-third',
    },
});