var gallery_frame, gallery_frame_main;

gallery_frame = document.getElementsByClassName('gallery-frame');

gallery_frame_main = document.getElementsByClassName('see-main-frame')[0];

my_events(gallery_frame, 'click', function($this) {

    gallery_frame_main.classList.remove('animate-show');

    var $this_img;

    $this_img = $this.querySelector('img').getAttribute('src');

    repeat_for(gallery_frame, function($this_item) {

        $this_item.classList.remove('active');
    })

    setTimeout(()=> {

        gallery_frame_main.classList.add('animate-show');

        $this.classList.add('active');

        gallery_frame_main.querySelector('img').setAttribute('src', $this_img);
    }, 50)
})