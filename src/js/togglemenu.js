var menu_toggle;

menu_toggle = document.getElementsByClassName('header-toggle')[0];

menu_toggle.addEventListener('click', () => {

    if(!header_main.classList.contains('showmenu')) {

        header_main.classList.add('showmenu');
        header_main.classList.remove('hidemenu');
    }
    else {

        header_main.classList.add('hidemenu');
        header_main.classList.remove('showmenu');
    }
})