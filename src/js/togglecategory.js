var category_item, 
    category_this_height,
    categoty_callapse;

category_item = document.getElementsByClassName('navbar-item');

my_events(category_item, 'click', function($this, event) {

    categoty_callapse = $this.querySelector('.navbar-collapse');

    if(!categoty_callapse.contains(event.target) && window.innerWidth < 992) {

        category_this_height = $this.querySelector('.navbar-content').offsetHeight;

        if($this.classList.contains('activecategory')) {

            $this.classList.remove('activecategory');

            $this.querySelector('.navbar-collapse').style.cssText = `
                max-height: 0;
            `;
        }
        else if (window.innerWidth < 992) {

            repeat_for(category_item, function($item) {

                if($item.classList.contains('activecategory')) {

                    $item.classList.remove('activecategory');

                    $item.querySelector('.navbar-collapse').style.cssText = `
                        max-height: 0;
                    `;
                }
            })

            $this.querySelector('.navbar-collapse').style.cssText = `
                max-height: ${category_this_height}px;
            `;

            $this.classList.add('activecategory');
        }
    }

    if(!categoty_callapse.contains(event.target) && window.innerWidth >= 992) {

        if(!$this.classList.contains('activecategorydesctop')) {

            var desctop_audit = true;

            repeat_for(category_item, function($item) {

                if($item.classList.contains('activecategorydesctop')) {

                    desctop_audit = false;
                }
            })

            if(desctop_audit) {

                $this.querySelector('.navbar-collapse').style.cssText = `

                    visibility: visible;
                    opacity: 1;
                `;

                $this.classList.add('activecategorydesctop');

                document.body.classList.add('categoryback');
            }
        }
        else {

            $this.querySelector('.navbar-collapse').style.cssText = `

                visibility: hidden;
                opacity: 0;
            `;

            $this.classList.remove('activecategorydesctop');

            document.body.classList.remove('categoryback');
        }

        // ~ Клик вне елемента

        document.addEventListener('click', (event) => {

            var $element;

            $element = document.getElementsByClassName('activecategorydesctop')[0];

            if(!$element.contains(event.target) && $this.classList.contains('activecategorydesctop')) {

                $this.querySelector('.navbar-collapse').style.cssText = `

                    visibility: hidden;
                    opacity: 0;
                `;

                $this.classList.remove('activecategorydesctop');

                document.body.classList.remove('categoryback');
            }
        })
    }
})